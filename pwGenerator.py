import random

characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()+<>?"
passwordLength = int(input('Please enter the number of characters for the password length: '))
passwordResult = ""

for i in range(passwordLength):
    next_index = random.randrange(len(characters))
    passwordResult = passwordResult + characters[next_index]

print(passwordResult)
