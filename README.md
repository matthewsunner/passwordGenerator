# Python Password Generator

This application generates a password from a set of characteres based on the desired length provided by the user. The simple script uses a for loop to map through a set of characters to generate the password. 

Future Features: 
- Ability to allow or disallow special characters
- Functionality to show the user the estimated length of time (in minutes) that it would take a computer to crack the password
- exe packaging for Windows, dmg for MacOS 


If you have any questions, reach out at any time. 